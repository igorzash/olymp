# TODO


## Algebra
* [+] Prime number check
* [+] Factoring
* Greatest common divisor/Least common multiple

## Geometry
* is point in triangle

## Graph theory
* [+] BFS/DFS
* [+] Dijkstra
* Eulerian path
* Component
* Coloring
* Binary search

## Dynamic programming
* [+] Rabbit problem