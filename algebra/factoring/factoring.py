

def factor(num, history):
    if num == 1:
        return history

    if num % 2 == 0:
        return factor(num / 2, history + [2])

    for i in range(2, int(num ** 0.5) + 1):
        if num % i == 0:
            return factor(num / 2, history + [i])
    
    return history + [num]
