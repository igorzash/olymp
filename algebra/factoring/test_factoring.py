import unittest

from algebra.factoring.factoring import factor

class Test(unittest.TestCase):
    def test_factor(self):
        factor_data = {
            10: [2, 5],
            12: [2, 2, 3],
            64: [2, 2, 2, 2, 2, 2],
        }

        for num, valid_data in factor_data.items():
            self.assertListEqual(factor(num, []), valid_data)


if __name__ == '__main__':
    unittest.main()
