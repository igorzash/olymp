import unittest


from algebra.prime_number_check.prime_number_check import is_prime


class Test(unittest.TestCase):
    def test_primes(self):
        primes = [2, 3, 5, 7, 11, 13, 17, 19, 23, 29, 31]

        for prime in primes:
            self.assertTrue(is_prime(prime))
    
    def test_non_primes(self):
        non_primes = [1] + list(range(4, 30, 2)) + list(range(6, 40, 3))

        for non_prime in non_primes:
            self.assertFalse(is_prime(non_prime))


if __name__ == '__main__':
    unittest.main()