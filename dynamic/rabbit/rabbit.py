

def rabbit(n, k):
    count = [1, 1]

    for i in range(2, n + 1):
        if i <= k:
            count.append(count[i - 1] * 2)
            continue
        
        count.append(count[i - 1] * 2 - count[i - 1 - k])
    
    return count[n]
