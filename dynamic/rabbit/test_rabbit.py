import unittest


from dynamic.rabbit.rabbit import rabbit


class Test(unittest.TestCase):
    def test(self):
        test_data = [
            ((1, 3), 1),
            ((2, 7), 21),
            ((3, 10), 274),
        ]

        for data in test_data:
            self.assertEqual(rabbit(data[0][1], data[0][0]), data[1])
