from collections import deque


def bfs(graph, start):
    visited = set()
    queue = deque([start])

    while queue:
        node = queue.popleft()

        visited.add(node)

        neighbours = list(map(lambda x: x[0], filter(
            lambda x: x[1] > 0 and x[0] not in visited, enumerate(graph[node]))))

        queue.extend(neighbours)

    return visited
