def dfs(graph, start):
    visited = set()
    queue = [start]

    while len(queue) > 0:
        node = queue.pop(-1)

        visited.add(node)

        neighbours = list(map(lambda x: x[0], filter(
            lambda x: x[1] > 0 and x[0] not in visited, enumerate(graph[node]))))

        queue.extend(neighbours)

    return visited

