import unittest


from graph_theory.bfs_dfs.bfs import bfs
from graph_theory.bfs_dfs.dfs import dfs


class Test(unittest.TestCase):
    def test(self):
        test_data = [
            ([[1, 1], [1, 1]], 0, {0, 1}),
            ([[0, 1, 0, 0],
              [0, 0, 1, 0],
              [1, 0, 0, 0],
              [0, 0, 0, 0]], 0, {0, 1, 2})
        ]

        for data in test_data:
            self.assertSetEqual(bfs(data[0], data[1]), data[2])
            self.assertSetEqual(dfs(data[0], data[1]), data[2])


if __name__ == '__main__':
    unittest.main()
