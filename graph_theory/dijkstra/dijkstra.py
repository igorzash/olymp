from math import inf


def dijkstra(graph, start):
    visited = {start}

    distances = [inf] * len(graph)
    distances[start] = 0

    current_node = start

    while True:
        neighbours = list(filter(
            lambda x: x[1] != -1 and x[0] not in visited, enumerate(graph[current_node])))

        if len(neighbours) == 0:
            break

        for index, weight in neighbours:
            if distances[index] > distances[current_node] + weight:
                distances[index] = distances[current_node] + weight

        next_node = None
        next_node_distance = inf

        for index, distance in enumerate(distances):
            if index in visited:
                continue

            if next_node_distance > distance:
                next_node = index
                next_node_distance = distance

        current_node = next_node
        visited.add(current_node)

    return distances
