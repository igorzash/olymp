import unittest

from graph_theory.dijkstra.dijkstra import dijkstra


class Test(unittest.TestCase):
    def test(self):
        test_data = [
            ([[-1, 5, 6, -1, 7, -1],
              [5, -1, -1, 8, -1, 1],
              [6, -1, -1, -1, -1, 4],
              [-1, 8, -1, -1, 9, -1],
              [7, -1, -1, 9, -1, -1],
              [-1, 1, 4, -1, -1, -1]], 0, [0, 5, 6, 13, 7, 6])
        ]

        for data in test_data:
            self.assertListEqual(dijkstra(data[0], data[1]), data[2])


if __name__ == '__main__':
    unittest.main()
