def find_eulerian_path(graph):
    in_degree, out_degree = count_degrees(graph)

    print(in_degree, out_degree)

    if not has_eulerian_path(in_degree, out_degree):
        return None

    start_node = find_start_node(in_degree, out_degree)

    del in_degree

    return dfs(start_node, graph, out_degree, [])


def count_degrees(graph):
    in_degree = [0] * len(graph)
    out_degree = []

    for i in range(len(graph)):
        neighbours = list(filter(lambda x: x[1] != -1, enumerate(graph[i])))

        out_degree.append(len(neighbours))

        for neighbour in neighbours:
            in_degree[neighbour[0]] += 1

    return in_degree, out_degree


def has_eulerian_path(in_degree, out_degree):
    start_nodes, end_nodes = 0, 0

    for i in range(len(in_degree)):
        if abs(in_degree[i] - out_degree[i]) > 1:
            return False

        if in_degree[i] - out_degree[i] == 1:
            end_nodes += 1
        elif out_degree[i] - in_degree[i] == 1:
            start_nodes += 1
    
    print(start_nodes, end_nodes)

    return (start_nodes == 1 and end_nodes == 1) or (start_nodes == 0 and end_nodes == 0)


def find_start_node(in_degree, out_degree):
    start = 0

    for i in range(len(in_degree)):
        if out_degree[i] - in_degree[i] == 1:
            return i

        if out_degree[i] > 0:
            start = i

    return start


def dfs(at, graph, out_degree, path):
    while out_degree[at] > 0:
        out_degree[at] -= 1

        neighbours = list(map(lambda x: x[0], filter(lambda x: x[1] != -1, enumerate(graph[at]))))

        path = dfs(neighbours[out_degree[at]], graph, out_degree, path)

    return [at] + path



print(find_eulerian_path([[-1, -1, 1, -1, -1, -1],
                          [1, -1, -1, -1, -1, 1],
                          [-1, 1, -1, -1, -1, 1],
                          [-1, -1, 1, -1, -1, -1],
                          [-1, -1, -1, 1, -1, -1],
                          [-1, -1, -1, -1, 1, -1]]))

